import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { StaticRouter } from 'react-router-dom';

import { Person } from '../../../../lib/interfaces/Person';
import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { ProfileOverview } from '../../src/components/profileOverview/component';

storiesOf('ProfileOverview', module)
  .add('All', () => {
    const profile: Partial<Person> = {
      affliation: [
        { affliation: { name: 'Megadodo Publications' }, roleName: 'Field researcher' },
      ],
      // tslint:disable-next-line:max-line-length
      description: 'An experienced galactic hitch-hiker and a field researcher for the Guide itself, and not an out-of-work actor from Guildford.',
      name: 'Ford Prefect',
      sameAs: { roleName: 'Biography', sameAs: 'https://en.wikipedia.org/wiki/Ford_Prefect_(character)' },
    };
    const articles: Array<Partial<ScholarlyArticle>> = [
      {
        // tslint:disable-next-line:max-line-length
        description: 'In this article we discuss different approaches to calculating the optimal routes for intergalactic bypasses.',
        identifier: 'arbitrary-id',
        name: 'Route optimisations for intergalactic bypasses',
      },
      {
        // tslint:disable-next-line:max-line-length
        description: 'We discuss a novel approach to relocating dwellers of homes located in the path of future bypasses.',
        identifier: 'arbitrary-id',
        name: 'On the relocation of human debris',
      },
    ];

    return (
      <StaticRouter>
        <ProfileOverview
          url="arbitrary-url"
          person={profile}
          articles={articles}
        />
      </StaticRouter>
    );
  });
