import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const mockArticles = [
  { datePublished: 'arbitrary date', identifier: 'arbitrary-id', name: 'Arbitrary name' },
  { datePublished: 'arbitrary date', identifier: 'arbitrary-id', name: 'Arbitrary name' },
  { datePublished: 'arbitrary date', identifier: 'arbitrary-id', name: 'Arbitrary name' },
  { datePublished: 'arbitrary date', identifier: 'arbitrary-id', name: 'Arbitrary name' },
  { datePublished: 'arbitrary date', identifier: 'arbitrary-id', name: 'Arbitrary name' },
];

jest.mock('../../src/services/periodical', () => ({
  getRecentScholarlyArticles: jest.fn().mockReturnValue(Promise.resolve(mockArticles)),
}));
jest.mock('../../src/services/account', () => ({
  getProfiles: jest.fn().mockReturnValue(Promise.resolve([ 'arbitrary-account-id' ])),
}));

import { ActivityTicker } from '../../src/components/activityTicker/component';

// TODO For some reason the `getProfiles` promise never resolves,
// so this test doesn't reach the stage where it should make its assertions
it.skip('should not render anything when there are fewer than five new articles', (done) => {
  const fewMockArticles = [ mockArticles[0], mockArticles[1], mockArticles[2], mockArticles[3] ];
  const mockPeriodicalService = require.requireMock('../../src/services/periodical');
  mockPeriodicalService.getRecentScholarlyArticles.mockReturnValueOnce(Promise.resolve(fewMockArticles));

  const overview = shallow(<ActivityTicker />);

  setImmediate(() => {
    overview.update();

    expect(overview).toBeEmptyRender();

    done();
  });
});
